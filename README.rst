Demo: using CoAP in the browser in Rust via WebSockets
======================================================

This program is a brief demonstration of how a web application can contact a
CoAP server using the CoAP over WebSocket (RFC8323_) transport.

As the author is not aware of any public CoAP-over-WS transports, or even
server implementations, you can prepare to run this as follows:

1.

    Run libcoap_'s CoAP-over-TCP server in a separate directory::

        $ git clone https://github.com/obgm/libcoap.git
        $ cd libcoap
        $ ./autogen.sh && ./configure && make
        $ cd examples
        $ ./coap-server -v10

    (If this does not immediately say "created TCP endpoint", you might need to
    explicitly enable TCP as defaults changed).

    Alternatively, there is a branch of aiocoap_ that recently does suport
    CoAP over TCP.

2.

    Run the shipped CoAP TCP-to-WebSocket converter::

        $ python3 ws2tcp.py

    That program is not a full CoAP implementation, but utilizes the similarity
    between the TCP and WebSockets transports to create what the author
    believes to be a working approximation of a real CoAP-over-WebSockets
    server. (What is known to be incorrect there is passing on Max-Message-Size
    CSM options without compensating for the difference in message sizes due to
    extlen being expressed in TCP.)

3.

    Run the application::

        $ cargo +nightly web start --target wasm32-unknown-unknown

    Then, direct your web browser to <http://localhost:8000>, and observe the
    exchanges in the browser's console log.

    The application will

    * open a websocket to the default local address of ws2tcp,
    * send a CSM (CoAP-over-reliable-transports' mandatory configuration message),
    * send a request for /.well-known/core on the WebSocket server, and
    * display all incoming messages (expected: CSM and response).

Motivation / long-term goals
----------------------------

Applications built as single-page web sites could become an alternative to
native applications currently prevalent in home automation and IoT setups.

When these applications are expected to work independently of an internet
connection, they need to establish secure connections with end points based on
credentials available in the browser. coap+wss (CoAP over WebSockets) is likely
not to provide that trust, as a local gateway is unlikely to obtain a PKI
certiicate; moreover, they only protect the hop are not end-to-end unless the
device provides the WebSocket itself (which is unlikely for constrained
devices). OSCORE could provide such protection.

Rust (compiled to WebAssembly) was chosen as an implementation language because
it should lend itself better to implementing OSCORE, and because of the
author's current interest in the lanuage.

License
-------

All software in this project is published under the terms of the AGPL3.0_ or
any later version. If you think that more permissive terms should be applied,
contact the author at <chrysn@fsfe.org>.

.. _RFC8323: https://tools.ietf.org/html/rfc8323
.. _libcoap: https://libcoap.net/
.. _aiocoap: https://github.com/chrysn/aiocoap
.. _AGPL3.0: https://www.gnu.org/licenses/agpl-3.0.en.html
