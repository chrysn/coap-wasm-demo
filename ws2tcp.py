#!/usr/bin/env python3

import argparse
import asyncio
import functools

import websockets

def build_argparse():
    p = argparse.ArgumentParser()

    p.add_argument('--bind-host', help='Address to bind to (default: %(default)s)', default='localhost')
    p.add_argument('--bind-port', help='Port to bind to (default: %(default)s)', default='8683')
    p.add_argument('--forward-host', help='Address to forward to (default: %(default)s', default='localhost')
    p.add_argument('--forward-port', help='Port to forward to (default: %(default)s)', default='5683')

    return p

def make_length_explicit(message):
    original_length = message[0] >> 4
    if original_length != 0:
        raise ValueError("Message already has a length")
    tkl = message[0] & 0x0f
    options_length = len(message) - tkl - 2
    if options_length < 13:
        length = options_length
        extlength = b""
    elif options_length < 269:
        length = 13
        extlength = bytes((options_length - 13,))
    elif options_length < 65805:
        length = 14
        extlength = (options_length - 269).to_bytes(2, "big")
    else:
        length = 15
        extlength = (options_length - 65805).to_bytes(4, "big") # will raise on too long messages

    return bytes(((length << 4) + tkl,)) + extlength + message[1:]

async def forward(websocket, path, forward_host, forward_port):
    if path != '/.well-known/coap':
        raise ValueError("You have strayed off the path, the path is /.well-known/coap but you said %s" % path)

    fwd_reader, fwd_writer = await asyncio.open_connection(forward_host, forward_port)

    async def ws2tcp():
        while True:
            message = await websocket.recv()
            message = make_length_explicit(message)
            fwd_writer.write(message)

    async def tcp2ws():
        while True:
            data = await fwd_reader.read(1)

            length = data[0] >> 4
            tkl = data[0] & 0x0f

            if length == 13:
                extlength = await fwd_reader.read(1)
                length = 13 + extlength[0]
            elif length == 14:
                extlength = await fwd_reader.read(2)
                length = 269 + int.from_bytes(extlength, 'big')
            elif length == 15:
                extlength = await fwd_reader.read(4)
                length = 65805 + int.from_bytes(extlength, 'big')
            data = bytes((tkl,)) + await fwd_reader.read(1 + tkl + length)

            await websocket.send(data)

    try:
        t1 = asyncio.Task(ws2tcp())
        t2 = asyncio.Task(tcp2ws())

        await asyncio.wait([t1, t2], return_when=asyncio.FIRST_COMPLETED)
    finally:
        t1.cancel()
        t2.cancel()
        fwd_writer.close()
        websocket.close()

if __name__ == "__main__":
    args = build_argparse().parse_args()
    loop = asyncio.get_event_loop()
    bind_host = vars(args).pop('bind_host')
    bind_port = vars(args).pop('bind_port')
    configured_forward = functools.partial(forward,
            **vars(args)
            )
    loop.run_until_complete(websockets.serve(configured_forward, bind_host, bind_port))
    loop.run_forever()
