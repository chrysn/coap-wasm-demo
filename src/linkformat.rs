#[derive(Debug)]
struct LinkValue<'a> {
    href: &'a str,
    params: Vec<(&'a str, Option<&'a str>)>,
}

// generic w/rt `is_a!("x ")` for all x: whitespace is probably not even permitted, but my
// link_header module produces it

named!(link_value_list<&str, Vec<LinkValue>>, separated_list_complete!(is_a!(", "), link_value));
named!(link_value<&str, LinkValue>, do_parse!(href: _href >> params: many0!(_prefixed_link_param) >> (LinkValue { href: href, params: params })));
named!(_prefixed_link_param<&str, (&str, Option<&str>)>, do_parse!(is_a!("; ") >> p: link_extension >> (p))); // using link_extension rather than link_param because it's a sane generalization.
named!(link_extension<&str, (&str, Option<&str>)>,
    alt!(
        do_parse!(p: parmname >> char!('=') >> v: alt!(ptoken | quoted_string) >> (p, Some(v))) |
        do_parse!(p: parmname >> (p, None))
    )); // ignoring ext-name-star, it's broken in link-format
named!(parmname<&str, &str>, is_a!("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$&+-.^_`|~")); // actually 1* of those
named!(ptoken<&str, &str>, is_a!("!#$%&'()*+-./0123456789:<=>?@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`{|}~")); // actually 1* of those
named!(quoted_string<&str, &str>, delimited!(char!('"'), is_not!("\""), char!('"'))); // TODO: is_not -> qdtext | quoted-pair
named!(_href<&str, &str>, delimited!(char!('<'), uri_reference, char!('>')));
named!(uri_reference<&str, &str>, is_not!(">")); // it's much more complex than that, but should suffice for here

pub fn demo_link_format(text: &str)
{
    // this trick around nom not eating the last item will fail once we want to return parts with
    // lifetimes out of this function.
    let text = text.to_owned() + &",<>";
    let x = link_value_list(&text);
    let x = match x { Ok(x) => x, _ => {
        console!(error, format!("Parsing failed: {:?}", x));
        unreachable!()
    }};
    console!(log, format!("Matched {:?} {:?}", x.0, x.1));
}

/// Given a text in application/link-format, find the first URI that's observable and return it.
/// Actual URI handling is not implemented yet.
pub fn find_observable(text: &str) -> Option<String>
{
    // this trick around nom not eating the last item will fail once we want to return parts with
    // lifetimes out of this function.
    let text = text.to_owned() + &",<>";

    let items = link_value_list(&text);
    let items = match items {
        Err(_) => return None,
        Ok(x) => x.1
    };

    for i in items {
        for p in i.params {
            if p == ("obs", None) {
                return Some(i.href.to_owned())
            }
        }
    }
    None
}

pub fn demo() {
    demo_link_format("<foo>");
    demo_link_format("<foo>,<bar>");
    demo_link_format("<foo>,<bar>,<http://example.com/>,<x>");
    demo_link_format("<foo>;bar=baz");
    demo_link_format("<foo>;bar=baz;ct=\"40 65534\";obs,<bar>;qux=quux");
    demo_link_format(r#"<one>;1="1",<two>;obs;2="2",</three>;3="3""#);
}
