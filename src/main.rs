#[macro_use]
extern crate stdweb;

extern crate byteorder;
#[macro_use]
extern crate nom;


mod linkformat;

use stdweb::web::{WebSocket, IEventTarget};
use stdweb::traits::IMessageEvent;

use byteorder::ByteOrder;
use byteorder::BigEndian;

fn handle_readable_text(text: &str, token: &[u8], socket: &WebSocket)
{
    if token == "1234".as_bytes() {
        console!(log, "Trying to interpret it as link format (in a very broken parser)");
        let observable = linkformat::find_observable(text);
        match observable {
            None => console!(log, "No observable resource found"),
            Some(path) => {
                console!(log, "Could observe", &path);
                launch_observation(&path, socket);
            }
        }
    } else {
        console!(log, "Payload is UTF8:", text);
    }
}

fn launch_observation(path: &str, socket: &WebSocket) -> ()
{
    if &path[0..1] != "/" {
        console!(error, "Path does not start with a slash, leaving my hands off it");
        return;
    }
    let mut request: Vec<u8> = Vec::with_capacity(path.len() + 6); // sufficient if all path components are only a few bytes
    // GET with token "obs"
    request.extend(b"\x03\x01obs");
    // Observe: 0
    request.extend(b"\x60");

    // Uri-Path: ...
    let mut optdelta: u8 = 11 - 6;
    let mut path = &path[1..];
    loop {
        let (segmentsize, is_last) = match path.find('/') {
            Some(x) => (x, false),
            None => (path.len(), true)
        };
        let segmentdata = &path[..segmentsize];
        if !is_last {
            path = &path[segmentsize + 1..];
        }
        if segmentsize >= 13 {
            console!(error, "Can't encode option length extended yet");
            return;
        }
        request.push((optdelta << 4) | (segmentsize as u8));
        request.extend(segmentdata.as_bytes());

        optdelta = 0;

        if is_last {
            break
        }
    }
    socket.send_bytes(&request).unwrap();
}

fn decode_uint(optiondata: &[u8]) -> u64 {
    if optiondata.len() == 0 { 0 }
    else { BigEndian::read_uint(optiondata, optiondata.len()) }
}

fn main() {
    linkformat::demo();

    let s = WebSocket::new_with_protocols("ws://localhost:8683/.well-known/coap", &["coap"]);
    let s = match s { Ok(s) => s, _ => { console!(error, format!("Error creating socket: {:?}", s)); return; }};
    console!(log, "Unwrapped socket", &s);

    let s_on_demand = std::rc::Rc::new(std::cell::RefCell::new(s));

    let s = s_on_demand.borrow_mut();

//     s.set_binary_type(stdweb::webapi::web_socket::BinaryType::ArrayBuffer);
    js! { @{s.as_ref()}.binaryType = "arraybuffer"; };

    let s_for_message_event = s_on_demand.clone();
    s.add_event_listener(move |x: stdweb::web::event::SocketMessageEvent| {
        console!(log, "%cReceived message", "background-color:lime", &x);

        let s = s_for_message_event.borrow_mut();

        let data = x.data();

        let data: Vec<u8> = match data.into_array_buffer() { Some(d) => d, None => { console!(error, "Not an array buffer!"); return } }.into();

        if (data[0] >> 4) != 0 {
            console!(error, "Non-zero length indicated");
            return;
        }

        let tkl: usize = (data[0] & 0x0f).into();
        let tkstart: usize = 2;
        let token = &data[tkstart .. tkstart + tkl];
        let code = data[1];
        let code = (code >> 5, code & 0x1f);

        console!(log, format!("Code is {:?}, token is {:?}. Total length {}.", code, token, data.len()));

        let mut data = &data[tkstart + tkl..];
        let mut option: u32 = 0;
        while data.len() > 0 {
            if data[0] == 0xff {
                let payload = &data[1..];
                let payload_text = std::str::from_utf8(&payload);
                match payload_text {
                    Ok(text) => handle_readable_text(text, token, &s),
                    _ => console!(log, format!("Payload is {:?}", payload))
                }
                break;
            }
            let od: u32 = (data[0] >> 4).into();
            let ol: usize = (data[0] & 0x0f).into();

            if od >= 13 {
                console!(error, "Can't handle big option skips yet");
                return;
            }
            if ol >= 13 {
                console!(error, "Can't handle big option lengths yet");
                return;
            }
            let extlengths = 0;

            option += od;

            let optiondata = &data[1 + extlengths .. 1 + extlengths + ol];

            if code == (7, 1) && option == 2 {
                // CSM Max-Message-Size
                console!(log, format!("Option Max-Message-Size = {}", decode_uint(&optiondata)));
            } else if code.0 != 7 && option == 12 {
                console!(log, format!("Option Content-Format = {}", decode_uint(&optiondata)));
            } else if code.0 != 7 && option == 6 {
                console!(log, format!("Option Observe = {}", decode_uint(&optiondata)));
            } else if code.0 != 7 && option == 14 {
                console!(log, format!("Option Max-Age = {}", decode_uint(&optiondata)));
            } else {
                console!(log, format!("Option {} with data: {:?}", option, optiondata));
            }

            data = &data[1 + extlengths + ol..];
        }

    });

    let s_for_socketopen_event = s_on_demand.clone();
    s.add_event_listener(move |x: stdweb::web::event::SocketOpenEvent| {
        console!(log, "Socket open, starting to send messages", &x);

        // If this panics, the javascript event dispatcher was bad.
        let s = s_for_socketopen_event.borrow_mut();

        // We must send a CSM; this is the trivial 7.01 empty message.
        s.send_bytes(b"\x00\xe1").unwrap_or_else(
            |e| console!(error, format!("Failed to send CSM: {:?}", &e)));

        // GET .well-known/core with a token
        // (".well-known" has lenth 11 = 0xb, "core" has length 4)
        s.send_bytes(b"\x04\x011234\xbb.well-known\x04core").unwrap_or_else(
            |e| console!(error, format!("Failed to send request: {:?}", e)));
    });
}
